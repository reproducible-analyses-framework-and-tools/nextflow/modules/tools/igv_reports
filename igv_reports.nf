process igv_reports {

    tag "${dataset}/${pat_name}"

    label 'igv_reports_container'
    label 'igv_reports'

    publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/igv_reports"

    input:
    tuple val(pat_name), val(dataset), path(norm_bam), path(norm_bai), path(tumor_bam), path(tumor_bai), path(rna_bam), path(rna_bai), path(bed)
    tuple path(fasta), path(faidx)

    output:
    tuple val(pat_name), val(dataset), path("*html")

    script:
    """
    create_report \
    ${bed} \
    --fasta ${fasta} \
    --tracks *bam \
    --output ${dataset}-${pat_name}.igvjs_viewer.html
    """
}
